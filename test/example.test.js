const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects... etc.
        console.log("Initializing tests");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3 for some reason??!?");
    });
    it("Division by zero", () => {
        // Tests
        expect(mylib.divide(8,0)).equal(undefined, "division by 0 should be undefined");
    });
    it("Can subtract 3 from 0", () => {
        // Tests
        expect(mylib.subtract(0,3)).equal(-3, "0 - 3 is not -3 for some reason? ;(");
    });
    it("Can multiply 10 by 69", () => {
        // Tests
        expect(mylib.multiply(10,69)).equal(690, "10 multiplied by 69 is not 690, mayday!");
    });
    after(() => {
        // Cleanup
        // For example: Shut down express server
        console.log("Testing completed.")
    });
});